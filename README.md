# Calculator 

## Prerequisites
- Ensure node js > version 8.0.0 is installed .


## TO run the app 
- The app is split into two :
    - The clint - which is a react Js frontend 
    - The backend which consists of both the commandline interface and the endpoint. 
- To run the backend , go to the root of the project directory and run `npm start` this starts both the cli and the endpoint.
- to start the frontend , run `npm start` from inside the client folder inside the main app directory.
- To run tests from inside the root directory run `npm test`.


## About
-	The application should add, subtract, divide,find remainder and multiply any two numbers.
-	The application should display a warning and exit if it receives any input that does not consist of numbers.
-	The system will provide a command line interface,an endpoint  that allows the end users to utilize program functionality.


### Endpoint and How it works.

- http://localhost:8080/{params}/?x={number1}&y={number2}
- The content in the curly braces should be replaced with actual operations for params it should be one of the following :-
 1. add
 2. substract
 3. multiply.
 4. divide.
 5. remeinder.
 6. exp 
- For numbers 1 and 2 it should be actual numbers between 1 and 10.

**Below is our list of files and folders.**

1.	calc.js
2.  node_modules
3.  operations.js
4.  package-lock.json
5.  package.json
6.  test/test.js
7.  client - cotains our UI code which is in react



## Future Improvements

- Use docker for ease of running the two applications 
- Add more test , especially on the UI .
- Use a storage facility.
- Add more functionality to the calculator.





 














