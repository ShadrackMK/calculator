const express = require('express');
const router = express.Router();

const operations = require('./operations.js');


router.get('/:operation', function(req, res) {
  const {x,y} = req.query;
  if (!operations.validateNumbers(x, y)) {
    res.status(201).send({message:"Only numbers are allowed!"});
  } else {
    let result;
     const {operation} = req.params;
    switch (operation) {
      case 'add':
          result = operations.add(x,y);
          res.status(200).send({answer:result});
        break;
      case 'subtract':
          result = operations.subtract(x,y);
          res.status(200).send({answer:result});
        break;
      case 'multiply':
       result = operations.multiply(x,y);
       res.status(200).send({answer:result});
        break;
      case 'divide':
        result = operations.divide(x,y);
        res.status(200).send({answer:result});
        break;
      case 'remainder':
         result = operations.remainder(x,y);
         res.status(200).send({answer:result});
        break;
      case 'exp':
       result = operations.exp(x,y);
       res.status(200).send({answer:result});
        break;
      default:
        res.status(201).send({message:'Please select the supported operations.'})
        break;
    }
  }
});

module.exports = router;
